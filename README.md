Description

A MovieApp API to manage movies, franchises, characters. Use CRUD requests for all entities, 
as well as usefull retrieval endpoints to get usefull data about each entity's relation to other entities.

  Requirements:
- Microsoft.EntityFrameworkCore.SQLserver
- Microsoft.EntityFrameworkCore.Design
- Microsoft.EntityFrameworkCore.Tools
- Swashbuckle.AspNetCore.Swagger
- Swashbuckle.AspNetCore.SwaggerGen
- Swashbuckle.AspNetCore.SwaggerUI
- Automapper.Extensions.Microsoft.DependencyInjection
