﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Profiles
{
    public class MovieCharacterProfile : Profile
    {
        public MovieCharacterProfile()
        {
        
            CreateMap<MovieCharacter, MovieNameDto>()
                .ForMember(mnd => mnd.Id, opt => opt
                .MapFrom(mc => mc.MovieId))
                .ForMember(mnd => mnd.MovieTitle, opt => opt
                .MapFrom(mc => mc.Movie.MovieTitle));
            CreateMap<MovieCharacter, CharacterNameDto>()
                .ForMember(cnd => cnd.Id, opt => opt
                .MapFrom(mc => mc.CharacterId))
                .ForMember(cnd => cnd.FullName, opt => opt
                .MapFrom(mc => mc.Character.FullName));
            CreateMap<MovieCharacter, MovieCharacterDto>()
                .ForMember(mcd => mcd.Character, opt => opt
                .MapFrom(mc => mc.Character.FullName))
                .ForMember(mcd => mcd.Movie, opt => opt
                .MapFrom(mc => mc.Movie.MovieTitle));

            CreateMap<MovieCharacter, AddChracterMovieDto>().ReverseMap();
            CreateMap<MovieCharacter, DeleteChracterMovieDto>().ReverseMap();

        }
    }
}
