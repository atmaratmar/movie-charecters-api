﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Franchise;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, ReadFranchiseDto>();
            CreateMap<Franchise, AddFranchiseDto>().ReverseMap();
            CreateMap<Franchise, UpdateFranchiseDto>().ReverseMap();
        }
    }
}
