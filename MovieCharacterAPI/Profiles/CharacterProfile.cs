﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, ReadCharacterDto>();
            CreateMap<Character, ReadChracterByIdDto>();
            CreateMap<Character, ChracterUpdateDto>().ReverseMap();
            CreateMap<Character, AddChracterDto>().ReverseMap();
        }
    }
}
