﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, ReadMovieDto>();
            CreateMap<Movie, MovieFranchiseDto>();
            CreateMap<Movie, AddMovie>().ReverseMap();
            CreateMap<Movie, UpdateMovieDto>().ReverseMap();
        }
    }
}
