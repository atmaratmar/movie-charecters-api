﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    ReleaseYear = table.Column<DateTime>(nullable: false),
                    Director = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    Trailer = table.Column<string>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    MovieId = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Strider", "Aragorn", "Male", "https://th.bing.com/th/id/OIP.TfBXL_neTaSexJaGonDDKwHaKJ?w=120&h=180&c=7&o=5&dpr=1.5&pid=1.7" },
                    { 2, "The Ringbearer", "Frodo Baggins", "Male", "https://th.bing.com/th/id/OIP.QBsuSnqfZCGKtNfWUusD3gHaFj?w=209&h=180&c=7&o=5&dpr=1.5&pid=1.7" },
                    { 3, "The Brave", "Samwise Gamgee", "Male", "https://th.bing.com/th/id/OIP.4cck6OXp8GXeyCvqR8J4AAHaK_?w=115&h=180&c=7&o=5&dpr=1.5&pid=1.7" },
                    { 4, "Im not a god damn wizard hAgRiD!", "Harry Potter", "Male", "https://th.bing.com/th/id/OIP.ImUwvW34nlsv-bpljXaIAwHaJT?w=124&h=180&c=7&o=5&dpr=1.5&pid=1.7" },
                    { 5, "Smartypants", "Hermoine Grainger", "Female", "https://i.pinimg.com/originals/8b/1a/73/8b1a7396a3ffa50b006a9338508540a7.jpg" },
                    { 6, "Lazer boi", "Luke Skywalker", "Male", "https://cdn-images-1.medium.com/max/1200/1*m2eh5I01_HudVv9WSVbN1Q.png" },
                    { 7, "Shot First", "Han Solo", "Male", "https://upload.wikimedia.org/wikipedia/en/b/be/Han_Solo_depicted_in_promotional_image_for_Star_Wars_%281977%29.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Cast it into the fire!", "The Lord of The Rings" },
                    { 2, "Yer a wizard Harry.", "Harry Potter" },
                    { 3, "Where are those heckin drones??", "Star Wars" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "Peter Jackson", 1, null, "The Fellowship of the Ring", "https://th.bing.com/th/id/OIP.Ph9A7p30xy_7P3LFYxWGswHaLH?pid=Api&rs=1", new DateTime(2000, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?time_continue=2&v=V75dMMIW2B4&feature=emb_title" },
                    { 2, "Peter Jackson", 1, null, "Two Towers", "https://th.bing.com/th/id/OIP.BluWPOxWqZ7B09CIMFKSWQHaI3?pid=Api&rs=1", new DateTime(2002, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=LbfMDwc4azU" },
                    { 3, "Peter Jackson", 1, null, "The Return of The King", "https://th.bing.com/th/id/OIP.VyFzglAb0N9KDANADkclkAHaLB?pid=Api&rs=1", new DateTime(2003, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=r5X-hFf6Bwo" },
                    { 4, "Chris Columbus", 2, null, "Harry Potter and The Philosophers Stone", "https://th.bing.com/th/id/OIP.WiNp8L0njssfE3waioCS6AHaKW?pid=Api&rs=1", new DateTime(2006, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=VyHV0BRtdxo" },
                    { 5, "Chris Columbus", 2, null, "Harry Potter and The Chamber of Secrets", "https://vignette.wikia.nocookie.net/harrypotter/images/c/c0/ALOExwKoxdkdeBvVi7NkaFl5Wa5.jpg/revision/latest?cb=20130803163017", new DateTime(2007, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=1bq0qff4iF8" },
                    { 6, "Alfonso Cuarón", 2, null, "Harry Potter and the Prisoner of Azkaban", "https://d3d8y6yhucfd29.cloudfront.net/sports-product-image/chris-columbus-signed-harry-potter-prisoner-of-azkaban-poster-8x10-proof-wcoa3-t6801424-1600.jpg", new DateTime(2008, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=lAxgztbYDbs" },
                    { 7, "George Lucas", 3, null, "A New Hope ", "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX140_CR0,0,140,209_AL_.jpg", new DateTime(1977, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=1g3_CFmnU7k" },
                    { 8, "Irvin Kershner", 3, null, "The Empire Strikes Back", "https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX140_CR0,0,140,209_AL_.jpg", new DateTime(1980, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=JNwNXF9Y6kY" },
                    { 9, "Richard Marquand", 3, null, "Return of The Jedi", "https://m.media-amazon.com/images/M/MV5BOWZlMjFiYzgtMTUzNC00Y2IzLTk1NTMtZmNhMTczNTk0ODk1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UX140_CR0,0,140,209_AL_.jpg", new DateTime(1983, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.youtube.com/watch?v=5UfA_aKBGMc" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "MovieId", "CharacterId", "Picture" },
                values: new object[,]
                {
                    { 1, 1, "picturestring" },
                    { 8, 7, "picturestring" },
                    { 8, 6, "picturestring" },
                    { 7, 7, "picturestring" },
                    { 7, 6, "picturestring" },
                    { 6, 5, "picturestring" },
                    { 6, 4, "picturestring" },
                    { 5, 5, "picturestring" },
                    { 5, 4, "picturestring" },
                    { 9, 6, "picturestring" },
                    { 4, 5, "picturestring" },
                    { 3, 3, "picturestring" },
                    { 3, 2, "picturestring" },
                    { 3, 1, "picturestring" },
                    { 2, 3, "picturestring" },
                    { 2, 2, "picturestring" },
                    { 2, 1, "picturestring" },
                    { 1, 3, "picturestring" },
                    { 1, 2, "picturestring" },
                    { 4, 4, "picturestring" },
                    { 9, 7, "picturestring" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
