﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class next : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Picture",
                table: "MovieCharacters");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Picture",
                table: "MovieCharacters",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 1, 1 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 1, 2 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 1, 3 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 2, 1 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 2, 2 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 2, 3 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 3, 1 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 3, 2 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 3, 3 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 4, 4 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 4, 5 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 5, 4 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 5, 5 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 6, 4 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 6, 5 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 7, 6 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 7, 7 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 8, 6 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 8, 7 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 9, 6 },
                column: "Picture",
                value: "picturestring");

            migrationBuilder.UpdateData(
                table: "MovieCharacters",
                keyColumns: new[] { "MovieId", "CharacterId" },
                keyValues: new object[] { 9, 7 },
                column: "Picture",
                value: "picturestring");
        }
    }
}
