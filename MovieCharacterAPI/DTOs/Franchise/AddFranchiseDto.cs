﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Franchise
{
    public class AddFranchiseDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
