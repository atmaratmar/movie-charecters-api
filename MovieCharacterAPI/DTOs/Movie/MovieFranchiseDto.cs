﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Movie
{
    public class MovieFranchiseDto
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public int FranchiseId { get; set; }
    }
}
