﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/Characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;
        public CharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all Characters
        /// </summary>
        /// <returns></returns>
        // GET: api/character
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadCharacterDto>>> GetCharacters()
        {
            var characters = await _context.Characters.ToListAsync();
            return StatusCode(200, characters.Select(c => _mapper.Map<ReadCharacterDto>(c)).ToList());
        }
        /// <summary>
        /// Get character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadChracterByIdDto>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }
            ReadChracterByIdDto ch = _mapper.Map<ReadChracterByIdDto>(character);

            return StatusCode(200,ch);
        }
        /// <summary>
        /// Get character's by franchise  id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("franchise/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterNameDto>>> GetFranchiseCharacters(int id)
        {
            var movieChars = await _context.MovieCharacters
                .Include(mc => mc.Movie)
                .ThenInclude(m => m.Franchise)
                .Include(mc => mc.Character)
                .Where(mc => mc.Movie.FranchiseId == id)
                .ToListAsync();
            if (movieChars == null)
            {
                return NotFound();
            }
            IEnumerable<CharacterNameDto> characterNameDtos = movieChars.Select(
                mc => _mapper.Map<CharacterNameDto>(mc)).ToList()
                .GroupBy(cad => cad.Id).Select(c => c.First());

            return StatusCode(200,characterNameDtos);
        }
        /// <summary>
        /// Update Character by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="chracterUpdate"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, ChracterUpdateDto chracterUpdate)
        {
            if (id != chracterUpdate.Id)
            {
                return BadRequest();
            }

            Character Ch_Update = _mapper.Map<Character>(chracterUpdate);
            _context.Entry(Ch_Update).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(201);
        }
        /// <summary>
        /// Add new Character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<AddChracterDto>> PostCharacter(AddChracterDto character)
        {
        
                Character NewCharacter = _mapper.Map<Character>(character);
                _context.Add(NewCharacter);
                await _context.SaveChangesAsync();

                return StatusCode(201, CreatedAtAction("GetCharacter", new { id = NewCharacter.Id }, character));            
        }
        /// <summary>
        /// Delete character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<ReadCharacterDto>> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
            var characterDto = _mapper.Map<ReadCharacterDto>(character);

            return StatusCode(202, characterDto);
        }
        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
