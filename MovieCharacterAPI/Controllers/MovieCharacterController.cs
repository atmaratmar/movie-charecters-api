﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mime;

namespace MovieCharacterAPI.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("v1/api/movie/character")]
    [ApiController]
    public class MovieCharacterController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;
        public MovieCharacterController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all movie and thier chrecters id 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterDto>>> GetMovieCharacters()
        {
            var movieCharacters = await _context.MovieCharacters.ToListAsync();
            return StatusCode(200, movieCharacters.Select(mc => _mapper.Map<MovieCharacterDto>(mc)).ToList());
        }     
        /// <summary>
        /// Add new character to movie 
        /// </summary>
        /// <param name="movieCharacter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<AddChracterMovieDto>> PostMovieCharacter(AddChracterMovieDto movieCharacter)
        {
            MovieCharacter NewFranchise = _mapper.Map<MovieCharacter>(movieCharacter);
            _context.Add(NewFranchise);
            await _context.SaveChangesAsync();
            return StatusCode(201,NewFranchise);
        }
        /// <summary>
        /// Delete a character from movie
        /// </summary>
        /// <param name="movieCharacter"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<DeleteChracterMovieDto>> DeleteMovieCharacter(DeleteChracterMovieDto movieCharacter)
        {
            var target = await _context.MovieCharacters.FindAsync(movieCharacter.MovieId, movieCharacter.CharacterId);
            if (target == null)
            {
                return NotFound();
            }
            _context.MovieCharacters.Remove(target);
            await _context.SaveChangesAsync();
            var movieCharacterDto = _mapper.Map<MovieCharacterDto>(target);
            return StatusCode(202,movieCharacterDto);
        }
    }
}
