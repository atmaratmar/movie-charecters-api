﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("v1/api/movie")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;
        public MoviesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadMovieDto>>> GetMovies()
        {
            var movies = await _context.Movies.ToListAsync();
            return StatusCode(200,movies.Select(m => _mapper.Map<ReadMovieDto>(m)).ToList());
        }
        /// <summary>
        /// Get movies by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadMovieDto>> GetMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }
            ReadMovieDto Mo = _mapper.Map<ReadMovieDto>(movie);
            return StatusCode(200,Mo);
        }
        /// <summary>
        /// Get all movies by franchise id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("franchise/{id}")]
        public async Task<ActionResult<IEnumerable<MovieFranchiseDto>>> GetFranchiseMovies(int id)
        {
            var movies = await _context.Movies
                .Where(m => m.FranchiseId == id).ToListAsync();
            if (movies == null)
            {
                return NotFound();
            }
            IEnumerable<MovieFranchiseDto> movieDtos = movies.Select(m => _mapper.Map<MovieFranchiseDto>(m));
            return StatusCode(200,movieDtos);
        }
        /// <summary>
        /// Update Movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, UpdateMovieDto movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }
            Movie Mo_Update = _mapper.Map<Movie>(movie);
            _context.Entry(Mo_Update).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(201);
        }
        /// <summary>
        /// Add new movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<AddMovie>> PostMovie(AddMovie movie)
        {
            Movie NewMovie = _mapper.Map<Movie>(movie);
            _context.Add(NewMovie);
            await _context.SaveChangesAsync();
            return StatusCode(201, CreatedAtAction("GetMovie", new { id = NewMovie.Id }, movie));
        }
        /// <summary>
        /// Delete movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<ReadMovieDto>> DeleteMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
            var movieDto = _mapper.Map<ReadMovieDto>(movie);
            return StatusCode(202, movieDto);
        }
        private bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
