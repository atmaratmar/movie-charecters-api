﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Franchise;
using MovieCharacterAPI.Model;

namespace MovieCharacterAPI.Controllers
{
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("v1/api/franchise")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;
        public FranchisesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        /// <summary>
        /// Get all frachise
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadFranchiseDto>>> GetFranchises()
        {
            var franchises = await _context.Franchises.ToListAsync();
            return StatusCode(200, franchises.Select(f => _mapper.Map<ReadFranchiseDto>(f)).ToList());
        }
        /// <summary>
        /// Get franchise by id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadFranchiseDto>> GetFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            ReadFranchiseDto Fr = _mapper.Map<ReadFranchiseDto>(franchise);
            return StatusCode(200,Fr);
        }
        /// <summary>
        /// Update franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, UpdateFranchiseDto franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }
            Franchise Fr_Update = _mapper.Map<Franchise>(franchise);
            _context.Entry(Fr_Update).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(201);
        }
        /// <summary>
        /// Add new franchise 
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<AddFranchiseDto>> PostFranchise(AddFranchiseDto franchise)
        {
            Franchise NewFranchise = _mapper.Map<Franchise>(franchise);
            _context.Add(NewFranchise);
            await _context.SaveChangesAsync();
            return StatusCode(201, CreatedAtAction("GetFranchise", new { id = NewFranchise.Id }, franchise));
        }
        /// <summary>
        /// Delete franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<ReadFranchiseDto>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
            var franchiseDto = _mapper.Map<ReadFranchiseDto>(franchise);
            return StatusCode(202, franchiseDto);
        }
        private bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
