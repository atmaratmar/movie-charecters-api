﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Model
{
    public class Character
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(15)]
        public string Gender { get; set; }
        [Url]
        public string Picture { get; set; }
        public ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}
