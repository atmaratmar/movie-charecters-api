﻿using Microsoft.EntityFrameworkCore;
using System;

namespace MovieCharacterAPI.Model
{
    public class MovieCharacterDbContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieCharacter> MovieCharacters { get; set; }

        public MovieCharacterDbContext(DbContextOptions options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId });

           
            modelBuilder.Entity<Franchise>().HasData(new { Id = 1, Name = "The Lord of The Rings", Description = "Cast it into the fire!"});
            modelBuilder.Entity<Franchise>().HasData(new { Id = 2, Name = "Harry Potter", Description = "Yer a wizard Harry." });
            modelBuilder.Entity<Franchise>().HasData(new { Id = 3, Name = "Star Wars", Description = "Where are those heckin drones??" });


            // LOTR
            modelBuilder.Entity<Movie>().HasData(new 
            { 
                Id = 1, 
                MovieTitle = "The Fellowship of the Ring",
                Genre = "action",
                ReleaseYear = new DateTime(2000, 01, 01),
                Director = "Peter Jackson",
                Picture = "https://th.bing.com/th/id/OIP.Ph9A7p30xy_7P3LFYxWGswHaLH?pid=Api&rs=1", 
                Trailer = "https://www.youtube.com/watch?time_continue=2&v=V75dMMIW2B4&feature=emb_title", 
                FranchiseId = 1});
            modelBuilder.Entity<Movie>().HasData(new
            {
                Id = 2,
                MovieTitle = "Two Towers",
                Genre = "action",
                ReleaseYear = new DateTime(2002, 01, 01),
                Director = "Peter Jackson",
                Picture = "https://th.bing.com/th/id/OIP.BluWPOxWqZ7B09CIMFKSWQHaI3?pid=Api&rs=1",
                Trailer = "https://www.youtube.com/watch?v=LbfMDwc4azU",
                FranchiseId = 1
            }); 
            modelBuilder.Entity<Movie>().HasData(new
            {
                Id = 3,
                MovieTitle = "The Return of The King",
                Genre = "action",
                ReleaseYear = new DateTime(2003, 01, 01),
                Director = "Peter Jackson",
                Picture = "https://th.bing.com/th/id/OIP.VyFzglAb0N9KDANADkclkAHaLB?pid=Api&rs=1",
                Trailer = "https://www.youtube.com/watch?v=r5X-hFf6Bwo",
                FranchiseId = 1
            });

            // HP
            modelBuilder.Entity<Movie>().HasData(new
            {
                Id = 4,
                MovieTitle = "Harry Potter and The Philosophers Stone",
                Genre = "action",
                ReleaseYear = new DateTime(2006, 01, 01),
                Director = "Chris Columbus",
                Picture = "https://th.bing.com/th/id/OIP.WiNp8L0njssfE3waioCS6AHaKW?pid=Api&rs=1",
                Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo",
                FranchiseId = 2
            });
            modelBuilder.Entity<Movie>().HasData(new
            {
                Id = 5,
                MovieTitle = "Harry Potter and The Chamber of Secrets",
                Genre = "action",
                ReleaseYear = new DateTime(2007, 01, 01),
                Director = "Chris Columbus",
                Picture = "https://vignette.wikia.nocookie.net/harrypotter/images/c/c0/ALOExwKoxdkdeBvVi7NkaFl5Wa5.jpg/revision/latest?cb=20130803163017",
                Trailer = "https://www.youtube.com/watch?v=1bq0qff4iF8",
                FranchiseId = 2
            }); 
            modelBuilder.Entity<Movie>().HasData(new
            {
                Id = 6,
                MovieTitle = "Harry Potter and the Prisoner of Azkaban",
                Genre = "action",
                ReleaseYear = new DateTime(2008, 01, 01),
                Director = "Alfonso Cuarón",
                Picture = "https://d3d8y6yhucfd29.cloudfront.net/sports-product-image/chris-columbus-signed-harry-potter-prisoner-of-azkaban-poster-8x10-proof-wcoa3-t6801424-1600.jpg",
                Trailer = "https://www.youtube.com/watch?v=lAxgztbYDbs",
                FranchiseId = 2
            });

            // Star Wars
            modelBuilder.Entity<Movie>().HasData(new
            {
                Id = 7,
                MovieTitle = "A New Hope ",
                Genre = "action",
                ReleaseYear = new DateTime(1977, 01, 01),
                Director = "George Lucas",
                Picture = "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX140_CR0,0,140,209_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=1g3_CFmnU7k",
                FranchiseId = 3
            });
            modelBuilder.Entity<Movie>().HasData(new
            {
                Id = 8,
                MovieTitle = "The Empire Strikes Back",
                Genre = "action",
                ReleaseYear = new DateTime(1980, 01, 01),
                Director = "Irvin Kershner",
                Picture = "https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX140_CR0,0,140,209_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=JNwNXF9Y6kY",
                FranchiseId = 3
            }); 
            modelBuilder.Entity<Movie>().HasData(new
            {
                Id = 9,
                MovieTitle = "Return of The Jedi",
                Genre = "action",
                ReleaseYear = new DateTime(1983, 01, 01),
                Director = "Richard Marquand",
                Picture = "https://m.media-amazon.com/images/M/MV5BOWZlMjFiYzgtMTUzNC00Y2IzLTk1NTMtZmNhMTczNTk0ODk1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UX140_CR0,0,140,209_AL_.jpg",
                Trailer = "https://www.youtube.com/watch?v=5UfA_aKBGMc",
                FranchiseId = 3
            });

            // Characters
            modelBuilder.Entity<Character>().HasData(new
            {
                Id = 1,
                FullName = "Aragorn",
                Alias = "Strider",
                Gender = "Male",
                Picture = "https://th.bing.com/th/id/OIP.TfBXL_neTaSexJaGonDDKwHaKJ?w=120&h=180&c=7&o=5&dpr=1.5&pid=1.7"
            });
            modelBuilder.Entity<Character>().HasData(new
            {
                Id = 2,
                FullName = "Frodo Baggins",
                Alias = "The Ringbearer",
                Gender = "Male",
                Picture = "https://th.bing.com/th/id/OIP.QBsuSnqfZCGKtNfWUusD3gHaFj?w=209&h=180&c=7&o=5&dpr=1.5&pid=1.7"
            });
            modelBuilder.Entity<Character>().HasData(new
            {
                Id = 3,
                FullName = "Samwise Gamgee",
                Alias = "The Brave",
                Gender = "Male",
                Picture = "https://th.bing.com/th/id/OIP.4cck6OXp8GXeyCvqR8J4AAHaK_?w=115&h=180&c=7&o=5&dpr=1.5&pid=1.7"
            });
            modelBuilder.Entity<Character>().HasData(new
            {
                Id = 4,
                FullName = "Harry Potter",
                Alias = "Im not a god damn wizard hAgRiD!",
                Gender = "Male",
                Picture = "https://th.bing.com/th/id/OIP.ImUwvW34nlsv-bpljXaIAwHaJT?w=124&h=180&c=7&o=5&dpr=1.5&pid=1.7"
            });
            modelBuilder.Entity<Character>().HasData(new
            {
                Id = 5,
                FullName = "Hermoine Grainger",
                Alias = "Smartypants",
                Gender = "Female",
                Picture = "https://i.pinimg.com/originals/8b/1a/73/8b1a7396a3ffa50b006a9338508540a7.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new
            {
                Id = 6,
                FullName = "Luke Skywalker",
                Alias = "Lazer boi",
                Gender = "Male",
                Picture = "https://cdn-images-1.medium.com/max/1200/1*m2eh5I01_HudVv9WSVbN1Q.png"
            });
            modelBuilder.Entity<Character>().HasData(new
            {
                Id = 7,
                FullName = "Han Solo",
                Alias = "Shot First",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/b/be/Han_Solo_depicted_in_promotional_image_for_Star_Wars_%281977%29.jpg"
            });

            // Actors
           
            // Binding table
            // fellowship
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 1,
                CharacterId = 1,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 1,
                CharacterId = 2,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 1,
                CharacterId = 3,
                Picture = "picturestring"
            });
            // twot towers
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 2,
                CharacterId = 1,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 2,
                CharacterId = 2,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 2,
                CharacterId = 3,
                Picture = "picturestring"
            });
            // return of the king
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 3,
                CharacterId = 1,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 3,
                CharacterId = 2,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 3,
                CharacterId = 3,
                ActorId = 3,
                Picture = "picturestring"
            });
            // philosophers stone
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 4,
                CharacterId = 4,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 4,
                CharacterId = 5,
                Picture = "picturestring"
            });

            // chamber of secrets
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 5,
                CharacterId = 4,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 5,
                CharacterId = 5,
                Picture = "picturestring"
            });

            // Azkaban
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 6,
                CharacterId = 4,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 6,
                CharacterId = 5,
                Picture = "picturestring"
            });

            // new hope
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 7,
                CharacterId = 6,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 7,
                CharacterId = 7,
                Picture = "picturestring"
            });

            // empire strikes back

            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 8,
                CharacterId = 6,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 8,
                CharacterId = 7,
                Picture = "picturestring"
            });

            // return of the jedi
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 9,
                CharacterId = 6,
                Picture = "picturestring"
            });
            modelBuilder.Entity<MovieCharacter>().HasData(new
            {
                MovieId = 9,
                CharacterId = 7,
                Picture = "picturestring"
            });
        }
    }
}
