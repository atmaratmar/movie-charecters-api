﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Model
{
    public class Movie
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string MovieTitle { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
        public DateTime ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [Url]
        public string Picture { get; set; }
        [Url]
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<MovieCharacter> MovieCharacters { get; set; }
    }
}
