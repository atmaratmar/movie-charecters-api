﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Model
{
    public class MovieCharacter
    {
       
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
        public int CharacterId { get; set; }
        public Character Character { get; set; }
    }
}
